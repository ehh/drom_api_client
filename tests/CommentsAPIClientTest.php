<?php
require __DIR__ . "/../CommentsAPIClient.php";
class CommentsAPIClientTest extends PHPUnit\Framework\TestCase
{
    private $classInstance;
    private $token;
    private $addConfig = ['name' => 'Test'];
    private $updateConfig = [
        'id' => 1,
        'name' => 'Test'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->token = sha1(microtime());
        $this->classInstance = new CommentsAPIClient();
        $this->addConfig['text'] = $this->token;
        $this->updateConfig['text'] = $this->token;
    }

    public function test__construct()
    {
        $this->assertSame(method_exists($this->classInstance, 'testServer'), true, "API client iInstance was not created properly!");
    }

    public function testConnection()
    {
        $this->assertTrue($this->classInstance->testServer(), "API server is not available!");
    }

    public function testMethods()
    {
        $this->assertNotFalse($this->classInstance->list(), "Method 'list' does not return anything!");
        $this->assertNotFalse($this->classInstance->add(self::ADD_CONFIG), "Method 'add' does not return anything!");
        $pos = strpos($this->classInstance->list(), $this->token);
        $this->assertNotFalse($pos, "Method 'add' does not add a record!");
        $this->assertNotFalse($this->classInstance->update(self::UPDATE_CONFIG), "Method 'update' does not return anything!");
        $this->assertFalse(strpos($this->classInstance->list(), $this->token) === $pos, "Method 'update' does not update values!");

    }

}