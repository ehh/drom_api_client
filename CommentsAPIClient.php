<?php


class CommentsAPIClient
{
    protected $url;
    protected $params;

    public function __construct($url = 'http://example.com/comments')
    {
        if($url){
            $this->url = $url;
        }
    }

    private function send($method = 'GET', $params = []){
        $requestParams = stream_context_create(
            [
                'http' => [
                    'method' => $method,
                    'content' => http_build_query($params)
                ]
            ]
        );
        return file_get_contents($this->url,  false, $requestParams);
    }

    public function list(){
        return $this->send();
    }

    public function add($params){
        return $this->send('POST', $params);
    }

    public function update($params){
        if(!isset($params['id'])){
            throw Exception("You must specify at least id in update method parameters!");
        }
        return $this->send('PATCH', $params);
    }

    public function testServer(){
        $data = false;
        try{
            $data = $this->list();
        } catch (Exception $e) {
            echo($e->getMessage());
            return false;
        }
        var_dump($data);

        if(!$data){
            return false;
        }

        return true;
    }
}

$comments = new CommentsAPIClient('http://local.portal.com/api/v1/');

